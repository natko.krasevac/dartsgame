﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoubleOutScript : MonoBehaviour
{

    public Toggle do_toggle;

    public void switch_toggle()
    {   
        int b = do_toggle.isOn ? 1 : 0;
        PlayerPrefs.SetInt("double_out", b);
    }
}
