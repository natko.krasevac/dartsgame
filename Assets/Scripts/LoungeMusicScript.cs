﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoungeMusicScript : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource source;

    void Awake()
    {
        source.clip = clip;
        source.Play();
    }
}
