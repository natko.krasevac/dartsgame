﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameSettings 
{
    public static int playersQuantity;
    public static string score;
    public static ClassicGame game;

    public static string toString()
    {
        return "GameSettings: [playersQuantity:" + playersQuantity.ToString() + "; gameType:" + score.ToString() + "]";
    }
}
