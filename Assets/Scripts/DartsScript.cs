﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DartsScript : MonoBehaviour
{
    public Image[] images;
    public int active;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        active = PlayerPrefs.GetInt("activeDarts");

        for (int i=0; i<3; i++)
        {
            if (i < active){
                images[i].enabled = true;
            }
            else images[i].enabled = false;
        }
    }
}
