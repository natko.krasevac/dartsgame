﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTypeDropdownFiller : MonoBehaviour
{
    public static Dictionary<string, ClassicGame> gameTypes = new Dictionary<string, ClassicGame>();
    public Dropdown gameTypeDropdown = null;

    // Start is called before the first frame update
    void Start()
    {
        List<Dropdown.OptionData> gameTypeDropdownOptions = new List<Dropdown.OptionData>();
        gameTypeDropdownOptions.Add(new Dropdown.OptionData("180"));
        gameTypeDropdownOptions.Add(new Dropdown.OptionData("301"));
        gameTypeDropdownOptions.Add(new Dropdown.OptionData("501"));

        gameTypeDropdown.AddOptions(gameTypeDropdownOptions);
    }

}
