﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateScoreboard : MonoBehaviour
{
    public Text[] scores = null;
    public Text[] players = null;

    private ClassicGame game;

    // Start is called before the first frame update
    void Start()
    {
        game = GetComponent<ClassicGame>();

        for (int i = 0; i < game.n_players; ++i)
        {
            scores[i].text = game.game_type.ToString();
        }

        for (int i = 0; i < game.n_players; ++i)
        {
            players[i].text = PlayerPrefs.GetString("name" + i.ToString());
        }
    }

    public void update(int player, int score)
    {
        scores[player].text = score.ToString();
    }
}
